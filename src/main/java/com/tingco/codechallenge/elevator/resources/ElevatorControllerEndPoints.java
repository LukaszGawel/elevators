package com.tingco.codechallenge.elevator.resources;

import com.tingco.codechallenge.elevator.api.IElevator;
import com.tingco.codechallenge.elevator.api.IElevatorController;
import com.tingco.codechallenge.elevator.impl.ElevatorNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 *
 */
@RestController
@RequestMapping("/rest/v1")
public final class ElevatorControllerEndPoints {

    private IElevatorController elevatorController;

    @Autowired
    public ElevatorControllerEndPoints(IElevatorController elevatorController) {
        this.elevatorController = elevatorController;
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {

        return "pong";
    }

    @RequestMapping(value = "/elevators", method = RequestMethod.GET)
    public ResponseEntity<List<IElevator>> getElevators() {
        return new ResponseEntity<>(elevatorController.getElevators(), HttpStatus.OK);
    }

    @RequestMapping(value = "/requestElevator/{toFloor}", method = RequestMethod.GET)
    public ResponseEntity<IElevator> requestElevator(@PathVariable("toFloor") int toFloor) {
        IElevator elevator = elevatorController.requestElevator(toFloor);
        elevator.moveElevator(toFloor);

        return new ResponseEntity<>(elevator, HttpStatus.OK);
    }

    @RequestMapping(value = "/elevatorsNextFloor", method = RequestMethod.GET)
    public ResponseEntity<List<IElevator>> moveElevators() {
        elevatorController.getElevators().forEach(IElevator::step);

        return new ResponseEntity<>(elevatorController.getElevators(), HttpStatus.OK);
    }

    @RequestMapping(value = "/releaseElevator/{id}", method = RequestMethod.GET)
    public ResponseEntity<IElevator> moveElevators(@PathVariable("id") int id) {
        Optional<IElevator> foundElevator = elevatorController.getElevators().stream()
                .filter(elevator -> elevator.getId() == id)
                .findFirst();
        if (foundElevator.isPresent()) {
            elevatorController.releaseElevator(foundElevator.get());
            return new ResponseEntity<>(foundElevator.get(), HttpStatus.OK);
        }
        throw new ElevatorNotFoundException(String.format("Elevator with id %s not found", id));
    }
}
