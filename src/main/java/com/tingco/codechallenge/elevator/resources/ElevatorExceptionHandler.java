package com.tingco.codechallenge.elevator.resources;

import com.tingco.codechallenge.elevator.impl.ElevatorNotFoundException;
import com.tingco.codechallenge.elevator.impl.WrongFloorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ElevatorExceptionHandler {

    @ExceptionHandler(ElevatorNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse noElevatorException(ElevatorNotFoundException ex) {
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(WrongFloorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse wrongFloorException(WrongFloorException ex) {
        return new ErrorResponse(ex.getMessage());
    }
}
