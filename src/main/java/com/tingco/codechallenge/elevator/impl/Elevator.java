package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.IElevator;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Elevator maintains 2 sets of floors:
 * - 1 TreeSet for requests from floors above currentFloor in ASC order.
 * - 1 TreeSet for requests from floors below currentFloor in DESC order.
 *
 * Initial state is that an elevator hasn't serviced any request and therfore has NONE direction.
 * It starts servicing request in the direction which has closest request floor and set the direction as UP or DOWN based on addressedFloor.
 * While moving UP / DOWN: move in this direction until all floor are serviced. When done servicing all floors reset the direction to NONE.
 *
 * TODO: introduce state (FUNCTIONAL, OUT_OF_ORDER). Only FUNCTIONAL elevators can be dispatched requests.
 * TODO: introduce MAX_CAPACITY. Only elevators which have capacity under MAX_CAPACITY can be serviced.
 */
@Component
public class Elevator implements IElevator {

    private final static Logger logger = Logger.getLogger(Elevator.class);
    private int id;
    private int currentFloor;
    private TreeSet<Integer> upDestinationFloors; // Sort ASC for up movement
    private TreeSet<Integer> downDestinationFloors; // Sort DESC for up movement
    private Direction direction;

    Elevator() {
        currentFloor = 0;
        upDestinationFloors = new TreeSet<>();
        downDestinationFloors = new TreeSet<>(Comparator.reverseOrder());
        direction = Direction.NONE;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    @Override
    public int getAddressedFloor() {
        if (direction == Direction.DOWN) {
            return downDestinationFloors.first();
        } else if (direction == Direction.UP) {
            return upDestinationFloors.first();
        }
        return 0;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void moveElevator(int toFloor) {
        if (toFloor > currentFloor) {
            upDestinationFloors.add(toFloor);
        } else {
            downDestinationFloors.add(toFloor);
        }
        logger.info(String.format("Elevator %s going %s - current floor %s. Person in, going to %s",
                id, direction.name(), currentFloor, toFloor));
        step();
    }

    @Override
    public boolean isBusy() {
        return direction != Direction.NONE;
    }

    @Override
    public int getCurrentFloor() {
        return currentFloor;
    }

    @Override
    public void step() {
        determineDirection();
        if (direction == Direction.UP) {
            if (upDestinationFloors.first() == currentFloor) {
                popUpDestination();
            }
            if (direction != Direction.NONE) {
                currentFloor++;
            }
        } else if (direction == Direction.DOWN) {
            if (downDestinationFloors.first() == currentFloor) {
                popDownDestination();
            }
            if (direction != Direction.NONE) {
                currentFloor--;
            }
        }
    }

    @Override
    public void release() {
        setDirection(Direction.NONE);
        if (upDestinationFloors.size() > 0) {
            upDestinationFloors.clear();
        }
        if (downDestinationFloors.size() > 0) {
            downDestinationFloors.clear();
        }
    }

    @Override
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    private void determineDirection() {
        if (direction == Direction.NONE) {
            if (upDestinationFloors.size() > 0 && downDestinationFloors.size() > 0) {
                if (Math.abs(currentFloor - upDestinationFloors.first()) < Math.abs(currentFloor - downDestinationFloors.first())) {
                    direction = Direction.UP;
                } else {
                    direction = Direction.DOWN;
                }
            } else if (upDestinationFloors.size() > 0) {
                direction = Direction.UP;
            } else if (downDestinationFloors.size() > 0) {
                direction = Direction.DOWN;
            }
        }
    }

    private void popUpDestination() {
        upDestinationFloors.remove(upDestinationFloors.first());
        logger.info(String.format("Elevator %s going %s - Person out at %s", id, direction.name(), currentFloor));

        if (upDestinationFloors.size() == 0) {
            if (downDestinationFloors.size() > 0) {
                direction = Direction.DOWN;
            } else {
                direction = Direction.NONE;
            }
        }
    }

    private void popDownDestination() {
        downDestinationFloors.remove(downDestinationFloors.first());
        logger.info(String.format("Elevator %s going %s - Person out at %s", id, direction.name(), currentFloor));

        if (downDestinationFloors.size() == 0) {
            if (upDestinationFloors.size() > 0) {
                direction = Direction.UP;
            } else {
                direction = Direction.NONE;
            }
        }
    }
}
