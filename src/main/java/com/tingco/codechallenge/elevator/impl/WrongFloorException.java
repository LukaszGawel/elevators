package com.tingco.codechallenge.elevator.impl;

public class WrongFloorException extends RuntimeException {

    public WrongFloorException(String message) {
        super(message);
    }
}
