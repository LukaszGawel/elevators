package com.tingco.codechallenge.elevator.impl;

public class ElevatorNotFoundException extends RuntimeException {

    public ElevatorNotFoundException(String message) {
        super(message);
    }
}
