package com.tingco.codechallenge.elevator.impl;

public enum Direction {
    UP, DOWN, NONE
}
