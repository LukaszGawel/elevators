package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.IElevator;
import com.tingco.codechallenge.elevator.api.IElevatorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Scheduling algorithm:
 * An elevator may run between MIN_FLOOR and MAX_FLOOR.
 * If there are two elevators going UP and DOWN direction choose the closest to the requested floor
 * If there is only one closest elevator choose this one to service the request.
 */
@Component
public class ElevatorController implements IElevatorController {

    private static final int MIN_FLOOR = 0;
    private static final int MAX_FLOOR = 10;
    private List<IElevator> elevators;

    @Autowired
    ElevatorController(@Value("${elevators.count}")int elevatorCount) {
        elevators = new ArrayList<>(elevatorCount);
        for (int i = 0; i < elevatorCount; i++) {
            IElevator elevator = new Elevator();
            elevator.setId(i);
            elevators.add(elevator);
        }
    }

    @Override
    public IElevator requestElevator(int toFloor) {
        if (toFloor < MIN_FLOOR || toFloor > MAX_FLOOR) {
            throw new WrongFloorException("Floor must be between 0 to 10");
        }
        int up = Integer.MAX_VALUE;
        int down = Integer.MAX_VALUE;
        IElevator upElevator = null;
        IElevator downElevator = null;
        IElevator requestElevator = null;

        for (IElevator elevator: elevators) {
            if (elevator.getCurrentFloor() < toFloor) {
                if (up > toFloor - elevator.getCurrentFloor()) {
                    up = toFloor - elevator.getCurrentFloor();
                    upElevator = elevator;
                }
            } else if (elevator.getCurrentFloor() > toFloor) {
                if (down > elevator.getCurrentFloor() - toFloor) {
                    down = elevator.getCurrentFloor() - toFloor;
                    downElevator = elevator;
                }
            }
        }

        // if found 2 elevators in both directions assign the request to the closest elevator.
        if (downElevator != null && upElevator != null) {
            if (down < up) {
                requestElevator = downElevator;
            } else {
                requestElevator = upElevator;
            }
        // if found only 1 closest elevator in down direction assign the request to it
        } else if (downElevator != null) {
            requestElevator = downElevator;
        // if found only 1 closest elevator in up direction assign the request to it
        } else if (upElevator != null) {
            requestElevator = upElevator;
        }
        if (requestElevator != null) {
            requestElevator.moveElevator(toFloor);
        }
        return requestElevator;
    }

    @Override
    public List<IElevator> getElevators() {
        return elevators;
    }

    @Override
    public void releaseElevator(IElevator elevator) {
        elevator.release();
    }

    @Override
    public void step() {
        elevators.forEach(IElevator::step);
    }
}
