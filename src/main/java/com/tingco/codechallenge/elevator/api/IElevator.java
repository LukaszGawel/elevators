package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.impl.Direction;

/**
 * Interface for an elevator object.
 *
 * @author Sven Wesley
 *
 */
public interface IElevator {

    /**
     * Tells which direction is the elevator going in.
     *
     * @return Direction Enumeration value describing the direction.
     */
    Direction getDirection();

    /**
     * If the elevator is moving. This is the target floor.
     *
     * @return primitive integer number of floor
     */
    int getAddressedFloor();

    /**
     * Get the Id of this elevator.
     *
     * @return primitive integer representing the elevator.
     */
    int getId();

    /**
     *Set id of the elevator
     * @param id
     */
    void setId(int id);

    /**
     * Command to move the elevator to the given floor.
     *
     * @param toFloor
     *            int where to go.
     */
    void moveElevator(int toFloor);

    /**
     * Check if the elevator is occupied at the moment.
     *
     * @return true if busy.
     */
    boolean isBusy();

    /**
     * Reports which floor the elevator is at right now.
     *
     * @return int actual floor at the moment.
     */
    int getCurrentFloor();

    /**
     * Sets an elevator direction
     * @param direction
     */
    void setDirection(Direction direction);

    /**
     * Runs an elevator one floor up/down.
     */
    void step();

    /**
     * Release an elevator
     */
    void release();

}
