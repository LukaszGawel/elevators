package com.tingco.codechallenge.elevator.api;

import java.util.List;


/**
 * Interface for the IElevator Controller.
 *
 * @author Sven Wesley
 *
 */
public interface IElevatorController {

    /**
     * Request an elevator to the specified floor.
     *
     * @param toFloor
     *            addressed floor as integer.
     * @return The IElevator that is going to the floor, if there is one to move.
     */
    IElevator requestElevator(int toFloor);

    /**
     * A snapshot list of all elevators in the system.
     *
     * @return A List with all {@link IElevator} objects.
     */
    List<IElevator> getElevators();

    /**
     * Telling the controller that the given IElevator is free for new
     * operations.
     *
     * @param IElevator
     *            the IElevator that shall be released.
     */
    void releaseElevator(IElevator IElevator);

    /**
     * Runs all elevators one floor up/down.
     */
    void step();

}
