package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.IElevator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ElevatorTest {

    private IElevator elevator;
    private final int id = 1;
    private final int toFloor = 2;

    @Before
    public void setUp() {
        elevator = new Elevator();
        elevator.setId(id);
        elevator.moveElevator(toFloor);
    }

    @Test
    public void getDirection() {
        Direction direction = elevator.getDirection();

        assertEquals("wrong elevator direction", Direction.UP, direction);
    }

    @Test
    public void getAddressedFloor() {
        int addressedFloor = elevator.getAddressedFloor();

        assertEquals("wrong addressed floor", toFloor, addressedFloor);
    }

    @Test
    public void getId() {
        int actualId = elevator.getId();

        assertEquals("wrong elevator id", id, actualId);
    }

    @Test
    public void moveElevator() {
        int currentFloor = elevator.getCurrentFloor();

        assertEquals("wrong current floor", 1, currentFloor);
    }

    @Test
    public void isBusy() {
        boolean isBusy = elevator.isBusy();

        assertTrue("elevator not busy", isBusy);
    }

    @Test
    public void currentFloor() {
        int currentFloor = elevator.getCurrentFloor();

        assertEquals("wrong current floor", 1, currentFloor);
    }

    @Test
    public void step() {
        elevator.step();
        int currentFloor = elevator.getCurrentFloor();

        assertEquals("wrong current floor", toFloor, currentFloor);
    }

    @Test
    public void setDirection() {
        elevator.setDirection(Direction.NONE);

        Direction direction = elevator.getDirection();

        assertEquals("wrong direction", Direction.NONE, direction);
    }
}