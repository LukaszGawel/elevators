package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.IElevator;
import com.tingco.codechallenge.elevator.api.IElevatorController;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ElevatorControllerTest {

    private IElevatorController elevatorController;

    @Before
    public void setUp() {
        elevatorController = new ElevatorController(1);
    }

    @Test
    public void requestElevator() {
        IElevator elevator = elevatorController.requestElevator(5);

        assertNotNull("No elevator", elevator);
    }

    @Test(expected = WrongFloorException.class)
    public void requestElevator_wrongFloor() {
        elevatorController.requestElevator(11);
    }

    @Test
    public void getElevators() {
        List<IElevator> elevators = elevatorController.getElevators();

        assertNotNull("No elevators", elevators);
        assertEquals("Elevators size not equal to 1", 1, elevators.size());
    }

    @Test
    public void releaseElevator() {
        IElevator elevator = elevatorController.getElevators().get(0);
        elevator.setDirection(Direction.UP);

        elevatorController.releaseElevator(elevator);

        assertEquals("Elevators direction is not NONE", Direction.NONE, elevator.getDirection());
    }
}