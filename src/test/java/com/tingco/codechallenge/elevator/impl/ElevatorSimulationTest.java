package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.IElevator;
import com.tingco.codechallenge.elevator.api.IElevatorController;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ElevatorSimulationTest {

    @Test
    public void run1Elevator() {
        ElevatorController elevatorController = new ElevatorController(1);
        elevatorController.requestElevator(2);
        elevatorController.requestElevator(6);
        elevatorController.requestElevator(4);
        elevatorController.requestElevator(1);
        elevatorController.requestElevator(8);
        elevatorController.requestElevator(2);
        elevatorController.requestElevator(0);
        elevatorController.requestElevator(10);

        runElevators(elevatorController);
    }

    @Test
    public void run2Elevators() {
        ElevatorController elevatorController = new ElevatorController(2);
        elevatorController.requestElevator(2);
        elevatorController.requestElevator(6);
        elevatorController.requestElevator(4);
        elevatorController.requestElevator( 1);
        elevatorController.requestElevator( 8);
        elevatorController.requestElevator( 2);
        elevatorController.requestElevator( 0);
        elevatorController.requestElevator( 10);

        runElevators(elevatorController);
    }

    @Test
    public void run3Elevators() {
        ElevatorController elevatorController = new ElevatorController(3);
        elevatorController.requestElevator(1);
        elevatorController.requestElevator(2);
        elevatorController.requestElevator(6);
        elevatorController.requestElevator(1);
        elevatorController.requestElevator(2);
        elevatorController.requestElevator(3);
        elevatorController.requestElevator(4);
        elevatorController.requestElevator(8);
        elevatorController.requestElevator(1);
        elevatorController.requestElevator(10);

        runElevators(elevatorController);
    }

    private void runElevators(ElevatorController elevatorController) {
        boolean isRunning = true;
        while (isRunning) {
            elevatorController.step();

            List<IElevator> stoppedElevators = elevatorController.getElevators().stream()
                    .filter(elevator -> elevator.getDirection() == Direction.NONE)
                    .collect(Collectors.toList());
            if (stoppedElevators.size() == elevatorController.getElevators().size()) {
                isRunning = false;
            }
        }
    }
}