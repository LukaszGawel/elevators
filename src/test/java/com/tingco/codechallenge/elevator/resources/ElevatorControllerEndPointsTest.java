package com.tingco.codechallenge.elevator.resources;

import com.tingco.codechallenge.elevator.config.ElevatorApplication;
import com.tingco.codechallenge.elevator.impl.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Boiler plate test class to get up and running with a test faster.
 *
 * @author Sven Wesley
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ElevatorApplication.class)
@AutoConfigureMockMvc
public class ElevatorControllerEndPointsTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getElevators() throws Exception {
        mvc.perform(get("/rest/v1/elevators")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[1].id", is(1)));
    }

    @Test
    public void requestElevator() throws Exception {
        mvc.perform(get("/rest/v1/requestElevator/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.currentFloor", is(2)))
                .andExpect(jsonPath("$.direction", is(Direction.UP.name())))
                .andExpect(jsonPath("$.addressedFloor", is(5)));
    }

    @Test
    public void requestElevator_badRequest() throws Exception {
        mvc.perform(get("/rest/v1/requestElevator/11")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Floor must be between 0 to 10")));
    }

    @Test
    public void elevatorsNextFloor() throws Exception {
        mvc.perform(get("/rest/v1/elevatorsNextFloor")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].currentFloor", is(3)))
                .andExpect(jsonPath("$[0].direction", is(Direction.UP.name())))
                .andExpect(jsonPath("$[0].addressedFloor", is(5)))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].currentFloor", is(0)))
                .andExpect(jsonPath("$[1].direction", is(Direction.NONE.name())))
                .andExpect(jsonPath("$[1].addressedFloor", is(0)));
    }

    @Test
    public void releaseElevator() throws Exception {
        mvc.perform(get("/rest/v1/releaseElevator/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.currentFloor", is(3)))
                .andExpect(jsonPath("$.direction", is(Direction.NONE.name())))
                .andExpect(jsonPath("$.addressedFloor", is(0)));
    }

    @Test
    public void releaseElevator_badRequest() throws Exception {
        mvc.perform(get("/rest/v1/releaseElevator/22")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Elevator with id 22 not found")));
    }
}
